package config_test

import (
	"testing"

	"gitlab.com/mayachain/midgard/config"
	"gitlab.com/mayachain/midgard/internal/db/testdb"
)

func TestMustLoadConfigFile(t *testing.T) {
	testdb.HideTestLogs(t)

	var c config.Config
	config.MustLoadConfigFiles("config.json", &c)
	config.LogAndcheckUrls(&c)
}
